# Decision Tree Workflows



A decision tree means that if they opened the email your system is going to take a specific action, and if they don’t open the email you take a different action. That’s just one example! You can have decision trees based on links within emails, assets that they’ve downloaded, or a variety of other tasks that the user might perform. Perhaps if they don’t open the email you wait 30 days and then send them another email just as a gentle reminder. The whole idea of drip flow campaigns and decision trees is to nurture your leads along a specific path.  



You nurture these leads along the path by assigning points to all of the activities they take. This means you can assign a point value if they visit a certain page, fill out a lead form, download a resource, or open an email. And these are just a few examples of ways in which you can assign points to your leads. The whole concept is designed to help create a value associated with each of your leads. So as your leads earn more points they become a more valuable lead. Eventually at a point total *that you define* that lead can be automatically moved into your CRM.