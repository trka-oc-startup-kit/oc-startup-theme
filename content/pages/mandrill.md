# Mandrill & Mautic
   ![](https://www.mautic.org/wp-content/uploads/2014/10/mandrill_128.png)
    ![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)
      
### About Mandrill
  Wherever you and your customers are, Mandrill can deliver your email in milliseconds. We’ve got servers all over the world.   

  Mandrill is a scalable and affordable email infrastructure service, with all the marketing-friendly analytics tools you’ve come to expect from Mailchimp.   

 [Learn more about Mandrill on their website](http://www.mandrill.com).  

   
### Integration Details
   Mandrill is one of several SMTP protocols allowed within the Mautic system and comes by default integrated within every Mautic installation.   

  You can enable Mandrill as your email service provider in the Configuration settings page.  Here you will enter your account details and other Mandrill specific information.