**Join Mautic Chats on Slack** Mautic uses Slack for live chats and welcomes you to join in. Go to [https://www.mautic.org/slack](/slack) to join!   

 Here are the teams currently available:  

   
   
#### [#general](https://mautic.slack.com/general)
 This is a general chat regarding all things Mautic  

  
#### [#core](https://mautic.slack.com/core)
 Focus is on the core development of Mautic.   

  
#### [#support](https://mautic.slack.com/support)
 This chat focuses on people helping people. Like having quick interactions? This is the place.  

  
#### [#dev](https://mautic.slack.com/dev)
 Building something cool for Mautic or with Mautic, this chat is here for discussions and support.  

  
#### [#meetups](https://mautic.slack.com/meetups)
 This chat is for organizers, planners, and event coordinators looking to form a MauticMeetup<sup>TM</sup>.   

  
#### [#docs](https://mautic.slack.com/docs)
 This chat is for those interested in improving Mautic documentation.  

  
#### [#design](https://mautic.slack.com/design)
 Discuss improvements and changes to the Mautic UI/UX and themes.  

  
#### [#translations](https://mautic.slack.com/translations)
 This chat is for those interested in translating Mautic into your native language. or who just want to connect with other translators.  

  
#### [#blog](https://mautic.slack.com/blog)
 Have an idea for the Mautic blog? Want to write a case study or best marketing practice article? Toss around ideas and discuss with the blog editors here.