# Ideas

Below you can see all the ideas our amazing members of the Mautic community (like you!) have proposed, and rate them. The highest rated ideas will be included in a future version, so if you have an idea [usernoise_link]tell us[/usernoise_link]!  



------

<form method="get" class="searchform themeform" action="https://www.mautic.org/" _lpchecked="1">
	<div>
		<input type="text" class="search form-control" name="s" onblur="if(this.value=='')this.value='To search type and hit enter';" onfocus="if(this.value=='To search type and hit enter')this.value='';" value="To search type and hit enter">
<input type="hidden" name="post_type" value="un_feedback">
	</div>
</form>

[All](https://www.mautic.org/ideas/all/)

[Accessibility](https://www.mautic.org/ideas/accessibility/)

[Campaigns](https://www.mautic.org/ideas/campaigns/)

[Emails](https://www.mautic.org/ideas/emails/)

[General](https://www.mautic.org/ideas/general/)

[Landing Pages](https://www.mautic.org/ideas/landing-pages/)

[Leads](https://www.mautic.org/ideas/leads-2/)

[Performance](https://www.mautic.org/ideas/performance/)

[Reports](https://www.mautic.org/ideas/reporting/)

[UI and Design](https://www.mautic.org/ideas/ui-design/)