Mautic is Open Source software distributed under the [GNU General Public License (GPL) Version 3](http://www.gnu.org/licenses/gpl-3.0.html).  The source code is freely available via GitHub.  

 [Browse Source Code](https://github.com/mautic/mautic)