# Helping you **understand** your audience, **listen** to them,
and **build** a lasting relationship.

[column size="one-third"]

![](https://www.mautic.org/wp-content/uploads/2014/08/highlight_check.png)


### **Easy to Setup**

Mautic makes installation a dream with a simple 3 step setup process. If you prefer something easier, you can use popular services to automatically install Mautic.


[/column][column size="one-third"]

![](https://www.mautic.org/wp-content/uploads/2014/08/highlight_check.png)


### **Complete Control**

You control your data with Mautic. No third-party services charging you to access your own information and keeping track of your data. Your contacts are your contacts.


[/column][column size="one-third" last="true"]

![](https://www.mautic.org/wp-content/uploads/2014/08/highlight_check.png)


### **Integrated Solutions**

Open source allows for unlimited integrations and customizations. Mautic integrates with dozens of solutions and can be easily extended via a powerful API.


[/column]




## **Sounds great**, but what does Mautic **do**?

Mautic provides detailed **contact tracking** along with powerful contact nurturing tools to help you **organize** your marketing campaigns. Mautic **automates** the process of finding and nurturing contacts through landing pages and forms, **sending email**, **text messages, web notifications, ****tracking social media,** and **integrating** with your CRM and other systems.  






## An Overview of the Workflow

This is where things get interesting! Let's walk through how Mautic can be used. Imagine a **new visitor** comes to your website. They are an IP address, nothing more, maybe some basic geographic data. They browse pages on your website, eventually landing on a special page with a **call-to-action** form. This form collects a few bits of information (*e.g. name and email address*). Now this visitor is now a **known visitor**. You may have a **contact segment** setup to capture these visitors and add them to a specific **campaign**. These campaigns will send emails, post to social media, and other actions based on either **specific times** or **user interaction**. Think of it like a decision tree. If the user opens the email, do *this*, otherwise do *that*. These campaigns can span hours, days, weeks, or even months!

Now are you starting to see what's happening? This known visitor is **gradually** becoming a **contact**. As this contact completes various actions, you give them **points**. These points help you to weight your contacts and see which ones are hot. Once a contact earns a certain amount of points they are **automatically moved** into your CRM or other system. Now you have a list of contacts you can confidently **know** are ready to be directly contacted.



------


## That's marketing automation. It's **simple**. It's **easy**. It's **automatic**.



## Mautic **Features** in Detail


[column size="one-half"]![manage leads](https://www.mautic.org/wp-content/uploads/2014/08/leads_list.png)
[/column]
[column size="one-half" last="true"]

### Contact Nurturing & Tracking



Mautic provides in-depth and detailed contact tracking along with powerful contact nurturing tools to provide businesses with the tools needed to organize their marketing campaigns. Contact tracking provides page views, time spent on site, and specific interests.

Mautic takes contact nurturing one step further by allowing businesses to create any type of additional custom fields for a contact. Using deeply integrated API’s it is possible to tie your CRM fields direct to your contact fields.


[/column]
[hr]
[column size="one-half"]

### Campaign Marketing & Dripflow Programs



Campaigns and drip programs are critical to any good marketing automation program. Mautic comes with flexible, easy-to-use campaign management and drip program creation. Create a campaign and define actions and events which contacts will follow. Automate the process of connecting with contacts and nurturing them along the sales funnel.

Mautic lets businesses define multiple outcome processes and set both date triggered actions as well as activity triggered actions.


[/column][column size="one-half" last="true"]![drip campaign marketing overview](https://www.mautic.org/wp-content/uploads/2014/08/campaign_overview.png)
[/column]
[hr]
[column size="one-half"]![social media marketing overview](https://www.mautic.org/wp-content/uploads/2014/08/social_overview.png)
[/column][column size="one-half" last="true"]

### Social Media Integrations



Social media networks provide fantastic opportunities to connect with contacts and potential customers. Businesses who recognize the importance of social media and how to use these networks to form relationships will find Mautic's integrations top of the line. In addition, Mautic allows developers to add any additional networks they choose.

Any social network with an API can be integrated into Mautic quickly and easily. Businesses love Mautic's social integrations.


[/column]
[hr]
[column size="one-half"]

### Landing Pages and Assets



Marketing automation uses landing pages to help capture key information from site visitors. Mautic lets businesses create stunning and unique landing pages easily through the integrated page builder. These landing pages provide key information to help monitor site traffic and also allows download monitoring for asset management.

All of these resources working together create a personal profile on each site visitor tailored to their interests and needs and provide businesses with relevant information for building a strong profile.


[/column][column size="one-half" last="true"]![marketing landing pages](https://www.mautic.org/wp-content/uploads/2014/08/landing_page.png)
[/column]


## What Makes Mautic **Different**?


[column size="one-half"]![marketing software beautiful interface](https://www.mautic.org/wp-content/uploads/2014/08/beautiful_interface.png)


### Beautiful Interface

Mautic is intuitive and incredibly easy-to-use.

[/column][column size="one-half" last="true"]![download free marketing software](https://www.mautic.org/wp-content/uploads/2014/08/self_saas.png)


### Self-Hosted or SaaS

Host your own Mautic or get free hosting.

[/column]


[column size="one-half"]![open source marketing](https://www.mautic.org/wp-content/uploads/2014/08/opensource_custom.png)


### Open Source & Custom

Mautic is GPL and fully customizable.

[/column][column size="one-half" last="true"]![marketing integrations](https://www.mautic.org/wp-content/uploads/2014/08/integrates.png)


### Integrate with Everything

Mautic integrates with hundreds of solutions.

[/column]


## Create **Stunning** Landing Pages

![amazing landing pages marketing](https://www.mautic.org/wp-content/uploads/2014/08/landing_pages.png)
  







## Some Technical Mautic Features


[column size="one-half"]




### **Themes**

Mautic lets you create, design, and distribute unique themes for landing pages and emails.  





[/column][column size="one-half" last="true"]




### **Marketplace**

Share or sell your add-ons, themes, and workflows through the Mautic Marketplace.  





[/column]


[column size="one-half"]




### **Core**

Contribute to the Mautic core. Become a part of the best open source community.  





[/column][column size="one-half" last="true"]




### **Integrations**

Create new integrations easily between Mautic and other software tools with Mautic Developer API.  





[/column]





## Managing Your First Campaign

Your first campaign is an exciting opportunity to put all the pieces into place and begin **automating** your marketing.
Remember automated campaigns are still **personal** campaigns.  




[column size="one-half"]
![marketing dripflow campaign step one](https://www.mautic.org/wp-content/uploads/2014/08/campaign_1.png)

[/column][column size="one-half" last="true"]

#### **Step 1. Create a Campaign**

The first step in creating your campaign is identifying the purpose and goal you want to accomplish. Maybe it's introducing your leads to a new product, or simply keeping in touch through a weekly newsletter.

[/column]


[column size="one-half"]
![marketing dripflow campaigns step two](https://www.mautic.org/wp-content/uploads/2014/08/campaign_2.png)

[/column][column size="one-half" last="true"]

#### **Step 2. Target A Contact Segment**

Once you decide the purpose of your campaign and the goals to accomplish the next step is to select the segment of contacts you want to target with this campaign. New contacts can be added a list at any point in time.

[/column]


[column size="one-half"]
![marketing drip campaign steps](https://www.mautic.org/wp-content/uploads/2014/08/campaign_3.png)

[/column][column size="one-half" last="true"]

#### **Step 3. Identify Your Actions**

One of the key parts of your campaign is to identify and create the actions you want to take during the campaign. These might include sending an email, posting to social media, or adding points to a lead based on decisions they make.

[/column]


[column size="one-half"]
![dripflow marketing campaigns](https://www.mautic.org/wp-content/uploads/2014/08/campaign_4.png)

[/column][column size="one-half" last="true"]

#### **Step 4. Define Your Outcomes**

Outcomes are the results of decisions made by your contacts targeted by the campaign along with any outcomes you manually define. Outcomes include moving a contact to a new segment, passing that contact to an integration, or creating another follow-up with that contact.

[/column]



## A **Beautiful** & **Powerful** User Interface

![mautic marketing automation software](https://www.mautic.org/wp-content/uploads/2014/08/mautic_screenshot.png)


The menu on the left allows you to get where you're going quickly. You can easily **import** contacts from other systems. Notice the **color bar** across the top of the leads, this helps you quickly identify a contact stage. You can also view contacts as **cards** or as a table. And everything happens with AJAX so it feels like a native application.

One screenshot could never give you the complete picture of all that Mautic can do. If you're **ready** to see more of the incredible user interface that makes Mautic such an **intuitive** and user-friendly platform then simply [download](https://mautic.org/download) the latest or setup a **free** [hosted](https://mautic.com) account.

Maybe you need **more information** but you don't have to take our word for it. Here's a few **testimonials** from our outstanding community. We're not picking just the best either! Come join the **discussion** in our Slack chat and talk to other people for yourself!

[hr]


> "I threw myself deep into it over the weekend and integrated it across everything we have and then started going crazy and coming up with some cool ways we could integrate it into other things. I love the thing."

- Peter


> "First off, I want to congratulate the Mautic team on this platform and the concept behind it. Simply incredible. For some time, I've wanted to figure out a way to leverage my advertising and digital marketing experience to help truly small business overcome the challenges of modern marketing. Mautic is going to fill a huge void."

- Camilo


> "I've been looking at Hubspot and Marketo in recent weeks, but after finding Mautic I've decided to go with it and have recommended it to our marketing department and GM. It is seriously good and I hope it continues to "kick ass"."

- Trevor


> "As an organization I believe your initiative is awesome, you are giving a huge value in a field that is currently growing and is also dominated by SaaS"

- Alvin





### **Seen Enough?** Get Started with Mautic Today!

There's no better time to take control of your contacts, your campaigns, your integrations and your business.  


[Download Now](/download) [Launch Hosted Account](https://mautic.com)