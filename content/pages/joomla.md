# Joomla & Mautic



![](https://www.mautic.org/wp-content/uploads/2014/10/joomla_128.png)



![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)







### About Joomla

Joomla is an award-winning content management system (CMS), which enables you to build Web sites and powerful online applications. Many aspects, including its ease-of-use and extensibility, have made Joomla the most popular Web site software available. Best of all, Joomla is an open source solution that is freely available to everyone.

[Learn more about Joomla on their website](http://www.joomla.org).




### Integration Details



The Mautic plugin provides an easy way for Joomla site administrators to implement the Mautic tracking pixel into their website.

Download the plugin and install through the Joomla administrator panel as you would any other plugin. Once you've installed the plugin you need to enter the URL to your Mautic installation and you're done.

[Download](https://github.com/mautic/mautic-joomla/archive/master.zip)
  
  

Using the Plugin

<script src="//fast.wistia.com/embed/medias/17waiugfox.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script>





  
  

Inserting Dynamic Web Content

Inserting Dynamic Web Content from a Campaign is easily accomplished:
`{mautic type="content" slot="slot_name"}default text{/mautic}`
You would replace "slot_name" with the name of the slot from your campaign.

Watch this video for more information.

<script src="//fast.wistia.com/embed/medias/crqsq7wwlg.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script>





  
  

Configuring the API

<script src="//fast.wistia.com/embed/medias/bcphslhznv.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script>