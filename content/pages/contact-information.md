Mautic is a community. We help each other and we work together to improve ourselves and the Mautic software.  




### Support

Individual email support is pretty much impossible when trying to help a global community. The best way to get support is to check the support forums where you may find someone else already has a solution to your problem or you can post a new thread and get feedback from others. You can also look through the documentation for answers if you prefer to discover things on your own.

### Security

If you think you’ve found a potential security threat within Mautic please follow these quick steps on reporting that problem:

**1. Verify the issue is a security issue. **
Lost passwords, lost access, additional features, or random bugs are not considered security issues. Also Mautic.org does not host anyone’s platform, Mautic.org provides the platform which users can install on their own server. Mautic.org therefore has no control over who uses the software or how they choose to use it. If your issue is a security issue please continue to step 2.

**2. Report the issue**
Please email security [at] mautic.org with as many of the details as possible. Typically you should be able to explain what the security hole is, how it can be manipulated, and how a tester can verify the existence of the problem.

### Getting Involved

There are many ways in which you can be involved with Mautic. Please review these areas on the [Get Involved page](index.php?option=com_content&view=article&id=23&catid=9&Itemid=120).