# Mailjet & Mautic



![](https://www.mautic.org/wp-content/uploads/2014/08/mailjet-v2_128.png)



![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)





 


### About Mailjet
  Mailjet is a powerful all-in-one email service provider and infrastructure that enables online platforms and services to send, deliver and track different types of transactional and marketing email to better optimize contacts send after send.  


Globally focused, Mailjet is available in over 4 different languages with follow-the-sun support, is data privacy compliant in both the US and Europe and fully loaded with powerful tools, APIs and integrations including Mautic, Google Cloud, Microsoft Azure, Wordpress and Segment. Mailjet serves over 32,000 clients worldwide such as Product Hunt, TagHeuer, MIT, Trivago, and Radisson Blu.  

 [Learn more about Mailjet on their website](http://www.mailjet.com).  

   
### Integration Details
   Mailjet is part of a selected number of SMTP protocols integrated within Mautic. When downloading Mautic, Mailjet is readily available as a preinstalled
email service provider option.  


How do you enable Mailjet within Mautic? Head to the Configuration Settings page to enter
your account details and additional Mailjet information.