const shared = require('../../support/shared')

describe('home page', function () {
	let pages_home = '/';

	beforeEach(() => {
		cy.visit(pages_home);
	})

	it('has a home nav', () => {
		cy.get(shared.data_sel('homenav'))
			.should('have.attr', 'href')
			.and('include', pages_home)
	});
	it('has a forum link', () => {
		cy.get(shared.data_sel('forum-button'))
			.click();
		cy.url().should('include', 'forum');

		cy.get('head > title')
			.should('contain', 'Get Answers');
	});
});

describe('user account', function () {
	describe('login', function () {
		let pages_login = [
			'login'
		].join('/');

		beforeEach(() => {
			cy.visit(pages_login)
		});
		afterEach(() => {
			cy.logout_link();
		});

		it('allows valid login', () => {
			let user = shared.test_users[0];
			cy.login_form(user.username, user.password);
			cy.get(shared.data_sel('user-name'))
				.should('contain', user.name);
		});
		it('redirects to forum', () => {
			let user = shared.test_users[0];
			cy.login_form(user.username, user.password);
			cy.url()
				.should('contain', '/forum')
		});
		it('rejects invalid login', () => {
			let user = shared.test_users[0];
			cy.get('[name=login]')
				.type(user.username);
			cy.get('[name=password]')
				.type('incorrect-password');
			cy.get('[type=submit]')
				.click({force: true})

			cy.url()
				.should('eq')
		});
	})
});

describe('forums', function () {
	describe('channel page', function () {
		let channel_page = [
			'forum',
			'channel',
			'general-discussion'
		].join('/') // a busy channel.

		beforeEach(() => {
			cy.visit(channel_page)
		})

		it('lists topics', () => {
			cy.get('tr.forum-topic')
				.should('have.length.above', 4)
		})
		it('pagination works', () => {
			cy.visit(`${channel_page}?page=2`);
			cy.get('tr.forum-topic')
				.should('have.length.above', 4)

			cy.get('.posts-total-summary > p')
				.should('contain', '21')
		})
	});
	describe('topic page', function () {
		let topic_page = [
			'forum',
			'topic',
			'how-to-trigger-a-campaign-visit-page-decision-through-using-wp-landing-page-url'
		].join('/') // a busy topic.
		beforeEach(() => {
			cy.visit(topic_page)
		})
	});
	describe('topic resource', () => {
		let channel_page = [
			'forum',
			'channel',
			'general-discussion'
		].join('/');

		it('can create new topic', () => {
			let user = shared.test_users[0];
			cy.login_form(user.username, user.password);
			cy.url().should('contain', '/forum'); // <-- using this redirect to wait for login to finish. is there a better way?
			//--
			cy.visit(channel_page);
			cy.get(shared.data_sel('newtopicbutton'))
				.click();
			//-- we're on the new topic page?
			cy.get('h4')
				.should('contain', 'Create a new discussion topic');
			//-- complete form
			let topic = {
				subject: 'cypress topic subject',
				content: 'cypress topic content',
			};
			cy.get('[name=subject]')
				.type(topic.subject);
			cy.get('[name=content]')
				.type(topic.content);
			cy.get('[type=submit]')
				.click();
			//--we redirected?
			cy.url()
				.should('contain', 'forum/topic');
			cy.get('h1#post-title')
				.should('contain', topic.subject);
			//--
			cy.logout_link();
		});

		it('can edit own topic', () => {
			let user = shared.test_users[0];
			let edit = " - additional content added during spec run";
			cy.find_own_first_topic(user.username, user.password);
			cy.get(shared.data_sel('post-edit-button'))
				.click();
			cy.get(shared.data_sel('topic-edit-content'))
				.type(edit);
			cy.get(shared.data_sel('topic-edit-submit'))
				.click();
			cy.get('.page-main')
				.should('contain', edit)
		})

		it('can remove own topic', () => {
			let user = shared.test_users[0];
			cy.find_own_first_topic(user.username, user.password);
			cy.get(shared.data_sel('post-edit-button'))
				.click();
			cy.get(shared.data_sel('post-edit-delete-button'))
				.should('be.visible');
		})

		it('anonymous cannot create new topic', () => {
			let pages_newtopic = '/forum/topic/default?channel=68635'
			cy.visit(pages_newtopic);
			cy.get('h4').should('contain', 'Create a new discussion topic')
			cy.get('[name=subject]').type('foo')
			cy.get('[name=content]').type('foo')
			cy.get('[type=submit]').click();
			cy.get('.alert.alert-danger').should('contain', 'You should be logged in.')
		})
	})
});

describe('mauticians', function () {
	describe('index page', function () {
		let pages_mauticianslist = [
			'mauticians'
		].join('/')

		beforeEach(() => {
			cy.visit(pages_mauticianslist)
		})

		it('lists users', () => {
			cy.get(shared.data_sel('user-card'))
				.should('have.length.above', 4)
		})

		it('pagination works', () => {
			cy.visit(`${pages_mauticianslist}?page=2`);
			cy.get(shared.data_sel('user-card'))
				.should('have.length.above', 4);
			cy.get('#pager-subtitle')
				.should('contain', '19 - 36');
		})
	});
});
