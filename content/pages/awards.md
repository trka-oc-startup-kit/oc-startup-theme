# Award Winning Marketing Software

Mautic truly is award winning open source marketing automation software and has a constantly growing list of awards and recognitions. Below is a listing of some of the awards that Mautic has won over the years in chronological order.

#### 2015

[Best Open Source Software Application](http://www.infoworld.com/article/2982622/open-source-tools/bossie-awards-2015-the-best-open-source-applications.html#slide7) - BOSSIES 2015 (InfoWorld)
Featured START (Collision Conference)
Top 10 Daily Trending (Product Hunt)

#### 2014

[2015 Startup to Watch](http://blog.betalist.com/post/startups-of-2015) (BetaList)[
](http://blog.betalist.com/post/startups-of-2015)Featured BETA (Web Summit)[
](http://blog.betalist.com/post/startups-of-2015)