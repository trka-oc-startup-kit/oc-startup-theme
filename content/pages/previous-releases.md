<table class="table table-bordered">
<thead>
<tr>
<th>Version</th>
<th>Release Date</th>
<th>Announcement</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://www.mautic.org/m/asset/9:mautic-123">1.2.3</a></td>
<td>2015-12-23</td>
<td><a href="https://www.mautic.org/community/index.php/1381-1-2-3-released">https://www.mautic.org/community/index.php/1381-1-2-3-released</a></td>
</tr>
<tr>
<td><a href="https://www.mautic.org/m/asset/8:mautic-122">1.2.2</a></td>
<td>2015-10-27</td>
<td><a href="https://www.mautic.org/community/index.php/1049-1-2-2-released">https://www.mautic.org/community/index.php/1049-1-2-2-released</a></td>
</tr>
<tr>
<td><a href="https://www.mautic.org/m/asset/7:mautic-121">1.2.1</a></td>
<td>2015-10-23</td>
<td><a href="https://www.mautic.org/community/index.php/1018-1-2-1-released">https://www.mautic.org/community/index.php/1018-1-2-1-released</a></td>
</tr>
<tr>
<td><a href="https://www.mautic.org/m/asset/6:mautic-120">1.2.0</a></td>
<td>2015-09-03</td>
<td><a href="https://www.mautic.org/community/index.php/727-1-2-0-released">https://www.mautic.org/community/index.php/727-1-2-0-released</a></td>
</tr>
<tr>
<td><a href="https://www.mautic.org/m/asset/5:mautic-113">1.1.3</a></td>
<td>2015-07-19</td>
<td><a href="https://www.mautic.org/community/index.php/542-1-1-3-released">https://www.mautic.org/community/index.php/542-1-1-3-released</a></td>
</tr>
<tr>
<td><a href="https://www.mautic.org/m/asset/4:mautic-112">1.1.2</a></td>
<td>2015-06-09</td>
<td><a href="https://www.mautic.org/community/index.php/428-1-1-2-released">https://www.mautic.org/community/index.php/428-1-1-2-released</a></td>
</tr>
<tr>
<td><a href="https://www.mautic.org/m/asset/3:mautic-111">1.1.1</a></td>
<td>2015-06-08</td>
<td><a href="https://www.mautic.org/community/index.php/420-1-1-1-released">https://www.mautic.org/community/index.php/420-1-1-1-released</a></td>
</tr>
<tr>
<td><a href="https://www.mautic.org/m/asset/2:mautic-110">1.1.0</a></td>
<td>2015-06-07</td>
<td><a href="https://www.mautic.org/community/index.php/414-1-1-0-released">https://www.mautic.org/community/index.php/414-1-1-0-released</a></td>
</tr>
<tr>
<td><a href="https://www.mautic.org/m/asset/1:mautic-105">1.0.5</a></td>
<td>2015-05-05</td>
<td><a href="https://www.mautic.org/community/index.php/325-1-0-5-released">https://www.mautic.org/community/index.php/325-1-0-5-released</a></td>
</tr>
<tr>
<td>1.0.4</td>
<td>2015-05-04</td>
<td><a href="https://www.mautic.org/community/index.php/324-1-0-4-released">https://www.mautic.org/community/index.php/324-1-0-4-released</a></td>
</tr>
<tr>
<td>1.0.3</td>
<td>2015-04-14</td>
<td><a href="https://www.mautic.org/community/index.php/278-1-0-3-released">https://www.mautic.org/community/index.php/278-1-0-3-released</a></td>
</tr>
<tr>
<td>1.0.2</td>
<td>2015-04-03</td>
<td><a href="https://www.mautic.org/community/index.php/250-1-0-2-released">https://www.mautic.org/community/index.php/250-1-0-2-released</a></td>
</tr>
<tr>
<td>1.0.1</td>
<td>2015-03-10</td>
<td><a href="https://www.mautic.org/community/index.php/183-1-0-0-released">https://www.mautic.org/community/index.php/183-1-0-0-released</a></td>
</tr>
<tr>
<td>1.0.0</td>
<td>2015-03-10</td>
<td><a href="https://www.mautic.org/community/index.php/183-1-0-0-released">https://www.mautic.org/community/index.php/183-1-0-0-released</a></td>
</tr>
</tbody>
</table>