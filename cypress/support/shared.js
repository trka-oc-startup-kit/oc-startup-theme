const base_url = "http://127.0.0.1"; //-- deprecated. cypress just does this

const data_sel = (id) => {
	return `[data-test=${id}]`
}

const test_users = [
	{
		name: 'Cypress 001',
		email: 'ard.kevin.84+cypress_user_001@gmail.com',
		username: 'cypress_user_001',
		password: 'cypress_user_001',
	},
	{
		name: 'Cypress 002',
		email: 'ard.kevin.84+cypress_user_002@gmail.com',
		username: 'cypress_user_002',
		password: 'cypress_user_002',
	},
];

const frontend_admin_user = {
	name: 'Kevin Ard',
	email: 'ard.kevin.84@gmail.com',
	username: 'ard.kevin.84@gmail.com',
	password: '2013ml002992',
};

module.exports = {
	base_url,
	data_sel,
	test_users,
	frontend_admin_user
}
