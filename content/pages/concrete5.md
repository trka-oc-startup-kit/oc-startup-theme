# Concrete5 & Mautic
   ![](https://www.mautic.org/wp-content/uploads/2014/10/concrete5_128.png)
    ![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)
      
### About Concrete5
  With concrete5, you get the best of both worlds. Anyone can start making their own website in seconds, and the editing experience is easy; just click on what you want to change. Developers still get a flexible and robust framework for building sophisticated web applications. With concrete5, however, site owners will be able to make changes and additions on their own, for years to come.  

 [Learn more about Concrete5 on their website](http://www.concrete5.org).  

   
### Integration Details
   The Mautic plugin provides an easy way for Concrete5 site administrators to implement the Mautic tracking pixel into their website.   

Download the plugin and install through the Concrete5 administrator panel as you would any other plugin. Once you've installed the plugin you need to enter the URL to your Mautic installation and you're done.    

 [Download](https://github.com/mautic/mautic-concrete5/archive/master.zip)