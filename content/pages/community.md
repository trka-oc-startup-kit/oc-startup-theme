<div class="page-title py-5">
    <h1 class="h1 text-center">{{'How can we help you today?'|_}}</h1>
</div>

<div class="row">
    <div class="col">
        <div class="card">
        <div class="card-status bg-brand-purple"></div>
            <div class="card-body text-center">
                <h3 class="card-title">Find Answers</h3>
                <p>Just getting started or stumble across a problem?<br />Start here in your search for a solution.</p>
                <p><a href="#" class="btn btn-brand-purple btn-lg"><icon class="fe fe-search"></icon> Find Answers</a></p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
        <div class="card-status bg-brand-purple"></div>
            <div class="card-body text-center">
                <h3 class="card-title">Product Updates</h3>
                <p>Interested in what has been released lately?<br />Review updates to Mautic here.</p>
                <p><a href="#" class="btn btn-brand-purple btn-lg"><icon class="fe fe-rss"></icon> What's New</a></p>
            </div>
        </div>
    </div>
</div>

<div class="row row-cards">
    <div class="col-sm-6 col-lg-3">
        <div class="card p-3">
          <div class="d-flex align-items-center">
            <span class="stamp stamp-md bg-dark mr-3">
              <i class="fe fe-users"></i>
            </span>
            <div>
              <h4 class="m-0"><a href="javascript:void(0)">1,352 <small>Mauticians</small></a></h4>
              <small class="text-muted">163 registered today</small>
            </div>
          </div>
        </div>
     </div>
    <div class="col-sm-6 col-lg-3">
        <div class="card p-3">
          <div class="d-flex align-items-center">
            <span class="stamp stamp-md bg-dark mr-3">
              <i class="fe fe-headphones"></i>
            </span>
            <div>
              <h4 class="m-0"><a href="javascript:void(0)">62 <small>Language Translations</small></a></h4>
              <small class="text-muted">13 fully translated</small>
            </div>
          </div>
        </div>
     </div>
     
     <div class="col-sm-6 col-lg-3">
        <div class="card p-3">
          <div class="d-flex align-items-center">
            <span class="stamp stamp-md bg-dark mr-3">
              <i class="fe fe-triangle"></i>
            </span>
            <div>
              <h4 class="m-0"><a href="javascript:void(0)">32 <small>Mauticamps</small></a></h4>
              <small class="text-muted">1 meeting this month</small>
            </div>
          </div>
        </div>
     </div>
     
     <div class="col-sm-6 col-lg-3">
        <div class="card p-3">
          <div class="d-flex align-items-center">
            <span class="stamp stamp-md bg-dark mr-3">
              <i class="fe fe-shopping-bag"></i>
            </span>
            <div>
              <h4 class="m-0"><a href="javascript:void(0)">91 <small>Mixins</small></a></h4>
              <small class="text-muted">300 downloads today</small>
            </div>
          </div>
        </div>
     </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div class='card-body'>
                <div class="row">
                    <div class="col">
                        <div class="card">
                          <div class="card-header">
                            <h4 class="card-title">Latest Discussions</h4>
                          </div>
                          <table class="table card-table">
                            <tbody><tr>
                              <td width="1"><i class="fa fa-chrome text-muted"></i></td>
                              <td>Google Chrome</td>
                              <td class="text-right"><span class="text-muted">23%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-firefox text-muted"></i></td>
                              <td>Mozila Firefox</td>
                              <td class="text-right"><span class="text-muted">15%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-safari text-muted"></i></td>
                              <td>Apple Safari</td>
                              <td class="text-right"><span class="text-muted">7%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-internet-explorer text-muted"></i></td>
                              <td>Internet Explorer</td>
                              <td class="text-right"><span class="text-muted">9%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-opera text-muted"></i></td>
                              <td>Opera mini</td>
                              <td class="text-right"><span class="text-muted">23%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-edge text-muted"></i></td>
                              <td>Microsoft edge</td>
                              <td class="text-right"><span class="text-muted">9%</span></td>
                            </tr>
                          </tbody></table>
                        </div>
                    </div>
                    
                    <div class="col">
                        <div class="card">
                          <div class="card-header">
                            <h4 class="card-title">Recent Help Topics</h4>
                          </div>
                          <table class="table card-table">
                            <tbody><tr>
                              <td width="1"><i class="fa fa-chrome text-muted"></i></td>
                              <td>Google Chrome</td>
                              <td class="text-right"><span class="text-muted">23%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-firefox text-muted"></i></td>
                              <td>Mozila Firefox</td>
                              <td class="text-right"><span class="text-muted">15%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-safari text-muted"></i></td>
                              <td>Apple Safari</td>
                              <td class="text-right"><span class="text-muted">7%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-internet-explorer text-muted"></i></td>
                              <td>Internet Explorer</td>
                              <td class="text-right"><span class="text-muted">9%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-opera text-muted"></i></td>
                              <td>Opera mini</td>
                              <td class="text-right"><span class="text-muted">23%</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-edge text-muted"></i></td>
                              <td>Microsoft edge</td>
                              <td class="text-right"><span class="text-muted">9%</span></td>
                            </tr>
                          </tbody></table>
                        </div>
                    </div>
                    
                    <div class="col">
                        <div class="card">
                          <div class="card-header">
                            <h4 class="card-title">About the Community</h4>
                          </div>
                          <table class="table card-table">
                            <tbody><tr>
                              <td width="1"><i class="fa fa-chrome text-muted"></i></td>
                              <td>Organization</td>
                              <td class="text-right"><span class="text-muted">Tax Status 501(c)3</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-firefox text-muted"></i></td>
                              <td>Leadership</td>
                              <td class="text-right"><span class="text-muted">Who does what</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-safari text-muted"></i></td>
                              <td>Working Groups</td>
                              <td class="text-right"><span class="text-muted">Volunteers</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-internet-explorer text-muted"></i></td>
                              <td>History</td>
                              <td class="text-right"><span class="text-muted">How it began</span></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-opera text-muted"></i></td>
                              <td>Founder</td>
                              <td class="text-right"><span class="text-muted">DB Hurley</span></td>
                            </tr>
                          </tbody></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="row">
    <div class="col">
      <div class="list-group list-group-transparent mb-0">
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active text-brand-purple">
          <span class="icon mr-3"><i class="fe fe-at-sign"></i></span>Social <span class="ml-auto badge badge-brand-purple">19,300 <i class="pl-1 fe fe-users"></i></span>
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-twitter"></i></span>Twitter <span class="ml-auto badge badge-dark">1,300</span>
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-facebook"></i></span>Facebook <span class="ml-auto badge badge-dark">7,130</span>
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-linkedin"></i></span>LinkedIn <span class="ml-auto badge badge-dark">3,902</span>
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-slack"></i></span>Slack <span class="ml-auto badge badge-dark">4,620</span>
        </a>
      </div>
    </div>
    <div class="col">
      <div class="list-group list-group-transparent mb-0">
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active text-brand-purple">
          <span class="icon mr-3"><i class="fe fe-map-pin"></i></span>Getting Started
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-download-cloud"></i></span>Download & Install
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-settings"></i></span>Configuration
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-users"></i></span>Importing Contacts
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-git-pull-request"></i></span>First Campaign
        </a>
      </div>
    </div>
    <div class="col">
      <div class="list-group list-group-transparent mb-0">
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active text-brand-purple">
          <span class="icon mr-3"><i class="fe fe-award"></i></span>Most Active Mauticians
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center px-3">
          <span class="icon mr-3"><span class="avatar avatar-sm">RB</span></span> Ronald Baher <span class="ml-auto badge badge-dark">Forums</span>
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center px-3">
          <span class="icon mr-3"><span class="avatar avatar-sm">NP</span></span> Norman Pracht <span class="ml-auto badge badge-dark">Github</span>
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center px-3">
          <span class="icon mr-3"><span class="avatar avatar-sm">KA</span></span> Kevin Ard <span class="ml-auto badge badge-dark">Website</span>
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center px-3">
          <span class="icon mr-3"><span class="avatar avatar-sm">PL</span></span> Paul Linhardtst <span class="ml-auto badge badge-dark">Slack</span>
        </a>
      </div>
    </div>
    <div class="col">
      <div class="list-group list-group-transparent mb-0">
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active text-brand-purple">
          <span class="icon mr-3"><i class="fe fe-copy"></i></span>Legal
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-file-text"></i></span>Licensing
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe mtc-mautic"></i></span>Brand & Trademark
        </a>
        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
          <span class="icon mr-3"><i class="fe fe-globe"></i></span>GDPR
        </a>

      </div>
    </div>
</div>
<!--

------


#### Mauticians

Check out this area if you are interested in the many amazing volunteers that make up the Mautic community. If you have a specific need, the Mauticians directory is the perfect place to find a developer or other individual capable of helping you.  


[View Mauticians](/mauticians)

------


#### MautiCamps

Everything's better with friends and with a MautiCamp you can find others that share your love for Mautic. Local MautiCamps are your opportunity to learn more about all you can do with open source marketing automation. Find one near you.  


[Browse MautiCamps](/mauticamps)

------


#### Marketplace

Mautic offers a truly unique marketing platform complete with a marketplace for you to find additional mixins ready to be installed in your Mautic. Search for your favorite 3rd party integration or extend core functionality through a mixin.  


[Extend Mautic](/marketplace)
-->