### Mautic Logo Horizontal

[column size="one-fourth"]<span class="list-group-item"><img src="/media/logos/notagline/horizontal/Mautic_Logo_RGB_LB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/notagline/horizontal/Mautic_Logo_CMYK_LB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/notagline/horizontal/Mautic_Logo_RGB_LB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/notagline/horizontal/Mautic_Logo_RGB_LB.png)[/column]

[column size="one-fourth"]<span class="list-group-item mautic-bg"><img src="/media/logos/notagline/horizontal/Mautic_Logo_RGB_DB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/notagline/horizontal/Mautic_Logo_CMYK_DB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/notagline/horizontal/Mautic_Logo_RGB_DB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/notagline/horizontal/Mautic_Logo_RGB_DB.png)[/column]

[column size="one-fourth"]<span class="list-group-item"><img src="/media/logos/notagline/horizontal/Mautic_Logo_Monochrome_RGB_LB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/notagline/horizontal/Mautic_Logo_Monochrome_CMYK_LB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/notagline/horizontal/Mautic_Logo_Monochrome_RGB_LB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/notagline/horizontal/Mautic_Logo_Monochrome_RGB_LB.png)[/column]

[column size="one-fourth" last="true"]<span class="list-group-item mautic-bg"><img src="/media/logos/notagline/horizontal/Mautic_Logo_Monochrome_RGB_DB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/notagline/horizontal/Mautic_Logo_Monochrome_CMYK_DB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/notagline/horizontal/Mautic_Logo_Monochrome_RGB_DB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/notagline/horizontal/Mautic_Logo_Monochrome_RGB_DB.png)[/column]


### Mautic Logo Vertical


[column size="one-fourth"]<span class="list-group-item"><img src="/media/logos/notagline/vertical/Mautic_Logo_Vertical_RGB_LB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_CMYK_LB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_RGB_LB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_RGB_LB.png)[/column]

[column size="one-fourth"]<span class="list-group-item mautic-bg"><img src="/media/logos/notagline/vertical/Mautic_Logo_Vertical_RGB_DB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_CMYK_DB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_RGB_DB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_RGB_DB.png)[/column]

[column size="one-fourth"]<span class="list-group-item"><img src="/media/logos/notagline/vertical/Mautic_Logo_Vertical_Monochrome_RGB_LB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_Monochrome_CMYK_LB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_Monochrome_RGB_LB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_Monochrome_RGB_LB.png)[/column]

[column size="one-fourth" last="true"]<span class="list-group-item mautic-bg"><img src="/media/logos/notagline/vertical/Mautic_Logo_Vertical_Monochrome_RGB_DB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_Monochrome_CMYK_DB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_Monochrome_RGB_DB.svg#)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/notagline/vertical/Mautic_Logo_Vertical_Monochrome_RGB_DB.png)[/column]


### Mautic Logo


[column size="one-fourth"]<span class="list-group-item"><img src="/media/logos/logo/Mautic_Logo_LB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/logo/Mautic_Logo_LB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/logo/Mautic_Logo_LB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/logo/Mautic_Logo_LB.png)[/column]

[column size="one-fourth"]<span class="list-group-item mautic-bg"><img src="/media/logos/logo/Mautic_Logo_DB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/logo/Mautic_Logo_DB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/logo/Mautic_Logo_DB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/logo/Mautic_Logo_DB.png)[/column]

[column size="one-fourth"]<span class="list-group-item"><img src="/media/logos/logo/Mautic_Logo_Monochrome_LB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/logo/Mautic_Logo_Monochrome_LB.pdf)[SVG Vector <span class="text-muted small">(RGB)</span>](/media/logos/logo/Mautic_Logo_Monochrome_LB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/logo/Mautic_Logo_Monochrome_LB.png)[/column]

[column size="one-fourth" last="true"]<span class="list-group-item mautic-bg"><img src="/media/logos/logo/Mautic_Logo_Monochrome_DB.png"></span>[PDF Vector <span class="text-muted small">(CMYK)</span>](/media/logos/logo/Mautic_Logo_Monochrome_DB.pdf)[SVG Vector <span class="text-muted small">(CMYK)</span>](/media/logos/logo/Mautic_Logo_Monochrome_DB.svg)[PNG Web <span class="text-muted small">(RGB)</span>](/media/logos/logo/Mautic_Logo_Monochrome_DB.png)[/column]



------


### Mautic Colors

When you use Mautic in your artwork either print or web, you’ll look great and keep Mautic looking sharp if you use the official colors.  



[column size="one-third"]
      ![Mautic Blue Color](/media/logos/swatches/mautic_blue.png)

      
        
#### Mautic Blue

        
Pantone 7671C  

RGB 78, 94, 158  

CMYK 80 70 10 0  

Hex #4e5e9e
         


      
    
[/column][column size="one-third"]
      ![Mautic Gold Color](/media/logos/swatches/gold.png)

      
        
#### Gold

        
Pantone 124C  

RGB 253, 185, 51  

CMYK 30 0 90 0  

Hex #fdb933
         


      
    
[/column][column size="one-third" last="true"]
      ![Mautic teal Color](/media/logos/swatches/teal.png)

      
        
#### Teal

        
Pantone 3265C  

RGB 0, 180, 157  

CMYK 80 0 50 0  

Hex #00b49d
         


      
    
[/column]