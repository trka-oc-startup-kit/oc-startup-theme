# New Mautician Details


You are one of the best of the best. We are thrilled to have you as part of the Mautic community and we welcome you as a fellow Mautician. In order for us to send you awesome goodies and free swag please drop your details into the form below.  


<small><em>(*And of course we hate spam too!)</em></small>
------

<script type="text/javascript" src="https://mautic.org/m/form/generate.js?id=8"></script>