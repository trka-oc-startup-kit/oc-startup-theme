# Mautic loves partnering with others!


#### Hosting Partners
 Because we have an easy set of Mautic minimum requirements for servers to meet there are thousands of hosts which can accommodate your Mautic software. Sometimes trying to find the best one for your situation can be difficult. Different environments provide different experiences and we want your marketing automation experience to be second to none.   

  Our history and experience provides us a unique opportunity to help you find the perfect host for you. We have listed below a few of those partners which we believe provide you with the most value. If you choose to use one of these hosts and use the links below you may save a significant part of your hosting costs as an added benefit, others may help support Mautic.org. However, if you don’t need a complete hosting solution you should also consider a free marketing automation instance on [Mautic.com](http://mautic.com)  

[column size="one-fourth"]![](https://www.mautic.org/wp-content/uploads/2014/08/host_placeholder.png)

##### Is this you?
[/column][column size="one-fourth"]![](https://www.mautic.org/wp-content/uploads/2014/08/host_placeholder.png)

##### Is this you?
[/column][column size="one-fourth"]![](https://www.mautic.org/wp-content/uploads/2014/08/host_placeholder.png)

##### Is this you?
[/column] [column size="one-fourth" last="true"] ![](https://www.mautic.org/wp-content/uploads/2014/08/host_placeholder.png)

##### Is this you?
[/column]