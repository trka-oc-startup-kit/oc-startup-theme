# Amazon SES & Mautic
   ![](https://www.mautic.org/wp-content/uploads/2014/10/amazon_128.png)
    ![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)
      
### About Amazon SES
  Amazon Simple Email Service (Amazon SES) is a cost-effective outbound-only email-sending service built on the reliable and scalable infrastructure that Amazon.com has developed to serve its own customer base. With Amazon SES, you can send transactional email, marketing messages, or any other type of high-quality content and you only pay for what you use.   

 [Learn more about Amazon SES on their website](http://aws.amazon.com/ses).  

   
### Integration Details
   Amazon SES is one of several SMTP protocols allowed within the Mautic system and comes by default integrated within every Mautic installation.   

  You can enable Amazon SES as your email service provider in the Configuration settings page.  Here you will enter your account details and other Amazon SES specific information.