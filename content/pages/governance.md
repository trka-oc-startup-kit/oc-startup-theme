Rules and policies are helpful to establish general guidelines and principles as well as providing transparency in how situations are handled.  

  This article outlines the rules and policies which govern the Mautic Core team. These rules are effective on publication and all Mautic Core team members must agree to observe and abide by the listed rules and protocol.  

  
### Core Development Rules
 Development is open and available to any member of the Mautic community. All fixes and improvements are done through pull requests to the code. This code is open source and publicly available. Learn more about how to access this source code and begin development here.  

 Pull requests and code submissions are decided upon by the release leader and the core team. When a decision is not clearly evident then the following voting process will be implemented.  

 ------ 
#### Voting Policy
 Votes are cast by all members of the core team. Votes can be changed at any time during the discussion. Positive votes require no explanation. A negative vote must be justified by technical or objective logic. A core team member cannot vote on any code they submit.  

 
#### Merging Policy
 The voting process on any particular pull request must allow for enough time for review by the community and the core team. This involves a minimum of 2 days for minor modifications and minimum of 5 days for significant code changes. Minor changes involve typographical errors, documentation, code standards, minor CSS, javascript, and HTML modifications. Minor modifications do not require a voting process. All other submissions require a vote after the minimum code review period and must be approved by two or more core members (with no core members voting against).      


### Core Membership Application
 Core team members are based on a form of meritocracy. We actively seek to empower our active community members and those demonstrating increased involvement will be given everything needed for their continued success.  

  
### Core Membership Revocation
 A Mautic Core membership can be revoked for any of the following reasons:  

  
 - Refusal to follow the rules and policies listed herein - Lack of activity for the previous 6 months - Willful negligence or intent to harm the Mautic project - Upon decision of the project leader 
  Revoked members may re-apply for core membership following at 12 month period.