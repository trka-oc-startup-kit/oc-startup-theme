const shared = require('../../support/shared')

const channel_page = [
	'forum',
	'channel',
	'general-discussion'
].join('/'); // a busy channel.

const topic_page = [
	'forum',
	'topic',
	'how-to-trigger-a-campaign-visit-page-decision-through-using-wp-landing-page-url'
].join('/') // a busy topic.

describe('forums', function () {
	describe('channel page', () => {

		beforeEach(() => {
			cy.visit(channel_page)
		})

		it('lists topics', () => {
			cy.get('tr.forum-topic')
				.should('have.length.above', 4)
		})
		it('pagination works', () => {
			cy.visit(`${channel_page}?page=2`);
			cy.get('tr.forum-topic')
				.should('have.length.above', 4)

			cy.get('.posts-total-summary > p')
				.should('contain', '21')
		})
	});
	describe('topic page', () => {
		beforeEach(() => {
			cy.visit(topic_page)
		})
	});
	describe('admin actions', () => {
		beforeEach(() => {
			let user = shared.frontend_admin_user;
			cy.login_form(user.username, user.password);

			cy.url().should('contain', '/forum');
			cy.visit(topic_page)
		})

		it('admin can lock and unlock', () => {
			cy.get('[data-request=onLock]').click();
			cy.get('#topicControlPanel .text-danger .fe-lock').should('be.visible');
			cy.get('[data-request=onLock]').click();
			cy.get('#topicControlPanel .text-danger .fe-lock').should('not.be.visible');
		})
	});

	describe('topic resource', () => {
		context('authenticated', () => {
			it.only('has working controlpanel button', ()=>{
				let user = shared.test_users[0];
				cy.login_form(user.username, user.password);
				cy.visit(topic_page);
				cy.get('a[href="#postForm"]').should('be.visible');
				cy.get('a[href="#postForm"]').click();
				cy.get('#postForm').should('be.visible');
			})

			it('can create new topic', () => {
				let user = shared.test_users[0];
				cy.login_form(user.username, user.password);
				cy.visit(channel_page);
				cy.get(shared.data_sel('newtopicbutton'))
					.click();
				//-- we're on the new topic page?
				cy.get('h4')
					.should('contain', 'Create a new discussion topic');
				//-- complete form
				let topic = {
					subject: 'cypress topic subject',
					content: 'cypress topic content',
				};
				cy.get('[name=subject]')
					.type(topic.subject);
				cy.get('[name=content]')
					.type(topic.content);
				cy.get('[type=submit]')
					.click();
				//--we redirected?
				cy.url()
					.should('contain', 'forum/topic');
				cy.get('h1#post-title')
					.should('contain', topic.subject);
				//--
				cy.logout_link();
			});

			it('can edit own topic', () => {
				let user = shared.test_users[0];
				let edit = " - additional content added during spec run";
				cy.find_own_first_topic(user.username, user.password);
				cy.get(shared.data_sel('post-edit-button'))
					.click();
				cy.get(shared.data_sel('topic-edit-content'))
					.type(edit);
				cy.get(shared.data_sel('topic-edit-submit'))
					.click();
				cy.get('.page-main')
					.should('contain', edit)
			})

			it('can remove own topic', () => {
				let user = shared.test_users[0];
				cy.find_own_first_topic(user.username, user.password);
				cy.get(shared.data_sel('post-edit-button'))
					.click();
				cy.get(shared.data_sel('post-edit-delete-button'))
					.should('be.visible');
			})
		})

		context('not authenticated', () => {
			it('anonymous cannot create new topic', () => {
				let pages_newtopic = '/forum/topic/default?channel=68635'
				cy.visit(pages_newtopic);
				cy.get('h4').should('contain', 'Create a new discussion topic')
				cy.get('[name=subject]').type('foo')
				cy.get('[name=content]').type('foo')
				cy.get('[type=submit]').click();
				cy.get('.alert.alert-danger').should('contain', 'You should be logged in.')
			})
		})
	})
})
