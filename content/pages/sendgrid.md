# Sendgrid & Mautic
   ![](https://www.mautic.org/wp-content/uploads/2014/10/sendgrid_128.png)
    ![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)
      
### About Sendgrid
  SendGrid is the world's largest Email Infrastructure as a Service provider. Our email delivery service moves 2% of the world's non-spam email (over 14 billion emails/month) for more than 180,000 companies including technology leaders like Pinterest, Spotify, and Uber.   

 [Learn more about Sendgrid on their website](http://www.sendgrid.com).  

   
### Integration Details
   Sendgrid is one of several SMTP protocols allowed within the Mautic system and comes by default integrated within every Mautic installation.   

  You can enable Sendgrid as your email service provider in the Configuration settings page.  Here you will enter your account details and other Sendgrid specific information.