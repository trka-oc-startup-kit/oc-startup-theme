# Beginner's Guide to Marketing Automation Software


Okay so let’s take a very introductory look at marketing automation what it is, and how you can use it in your business. Always start with a question...**what is marketing automation?** Although the term of marketing automation may be a bit confusing or different to you I’m sure you are familiar with the concept. We will begin by looking at what exactly marketing automation is and then how you can apply these steps to your business.  




### Starting with an IP address

![](https://medium2.global.ssl.fastly.net/max/2000/1*bQaQFx5sJYA3gApXBYqYpw.jpeg)


When someone comes to your website they’re basically just an IP address. Yes, there may be some geographical data or other bits of generic information attached to this IP address but these visitors remain anonymous. You need to find some way of tracking these anonymous visitors and turning them into a potential lead for your business. So with market information the whole idea is your tracking all of these anonymous visitors to your website; keeping track of every page; watching how long they stay on each page and determine what will encourage them to take further action.

### 3 Big Next Steps

Let's break down each of the next three into their own section. Read them and come back or finish and then dive into the details later:

[column size="one-third"][![How to build a great landing page](https://www.mautic.org/wp-content/uploads/2015/07/above-736879_1280-300x204.jpg)
](https://www.mautic.org/marketing-automation-software/landing-page/)[Landing Pages to Collect More Data](https://www.mautic.org/marketing-automation-software/landing-page/)[/column][column size="one-third"][![Marketing drip campaign workflows](https://www.mautic.org/wp-content/uploads/2015/07/dripflow_campaign-300x219.jpg)
](https://www.mautic.org/marketing-automation-software/drip-marketing-campaign)[Beginning a Drip Marketing Campaign](https://www.mautic.org/marketing-automation-software/drip-marketing-campaign/)[/column][column size="one-third" last="true"][![Decision trees can be beautiful](https://www.mautic.org/wp-content/uploads/2015/07/decision_tree-300x200.jpg)
](https://www.mautic.org/marketing-automation-software/decision-tree-workflows/)[Create a Decision Tree](https://www.mautic.org/marketing-automation-software/decision-tree-workflows/)[/column]

Welcome back (if you left) or don't forget to come back and read these! Let's keep moving with next steps and one of the biggest results you're going to fall in love with when using marketing automation.

### Establish critical integrations

![](https://medium2.global.ssl.fastly.net/max/1696/1*zlr0L73MokYkgIs07avzpQ.png)


This is where it gets exciting! Now when your sales team logs in to your CRM system they are greeted with a list of qualified leads that have been nurtured along the process for however long that process took and they are now ready to be contacted. What this process does is save tremendous amounts of time for your entire sales team. No longer are your sales team members stuck cold calling all sorts of potential leads that haven’t been fully qualified or maybe only slightly interested because maybe they only visited the site once and downloaded a single asset. Instead they can spend their time calling those leads or contacting those leads who are qualified through the lead nurturing process that’s been done automatically by your marketing automation software.

### Save Time

This is a pretty exciting process. When a business implements marketing automation into their workflow they eliminate the tedious time-consuming tasks which consume far too much of their sales team members time. So when we look at the idea of marketing automation we’re not talking about an impersonal relationship with your leads. What we’re talking about instead is redefining those personal touches in the communication that occurs and having them automatically triggered.

> The contact is still personal but the timing has been automated.


------

[Mautic](https://www.mautic.org/) is an open source marketing automation software available completely for free and as software you can install on your own website or server. Access the latest, cutting-edge in marketing platforms without a high monthly fee. [Download](https://www.mautic.org/download) or [setup a free hosted account](https://mautic.com/).