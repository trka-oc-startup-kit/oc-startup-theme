# An Organizational Overview

It is most important to recognize the invaluable contributions of the Mautic community. The community is the heart and soul of Mautic and without the many volunteers dedicating their time, Mautic would not be possible. Below are the current appointed core team to help provide guidance and direction.  


Let’s start by looking at a high-level layout of the various positions and opportunities and then dig into the details for particular roles and responsibilities. 

Before you get overwhelmed, instead, get excited. This is an incredible time for you to get involved in something that’s bigger than yourself and contribute in a meaningful way. But first, we need a starting point. Your time is valuable; in fact, the most valuable resource you have. You should understand *why* Mautic is important and *what* our community believes to see if this is a place where you want to donate your time and talents.  Only after that will *how* you volunteer become important.

## Why volunteer with Mautic
The best way to describe Mautic is a close look at what we believe. The reasons that compel us to create. Here are a few of the foundational concepts we believe in as a community:

* Every organization should have an equal opportunity for success regardless of their size or financial status. 
* Open source code improves the world and encourages transparent communication; the more we know the better we can be.
* Working together is more powerful than working alone.
* Everyone can find greater financial success when the foundation is a powerful shared collaborated platform.
* Relationships are more important than revenues.
* Everyone is unique; our differences make us stronger.
* We always learn something new and grow personally by keeping an open mind and communicating empathetically with others.
* The details matter; we sweat the small stuff.
* Passion is encouraged. 

If you read the above list of values and beliefs our community is based upon and find yourself nodding in agreement, then keep reading, Mautic is a good place for you to get involved. We are always looking for fresh faces, and new friends.  And we’re eager to see you jump in and realize you’re one of us. You’re a Mautician. Welcome!

## Who should be interested in this opportunity
This post signifies a transition point in our community. We’ve grown large enough to take this exciting next step. This means there are realistically two audiences at this point in the conversation. First, we have those seasoned Mauticians who have weathered the storms, seen the highs and lows of our rapid growth and have demonstrated their commitment to their belief in our shared values. Second, we have all the new individuals, those volunteers who are just hearing about Mautic and all we are accomplishing. Whether it’s been a few weeks, few days, few hours, or few minutes since you learned about Mautic and read the values above, we’re glad you’re here and we’re eager to see what role you will play. 

Regardless of which group you identify with the next section is important for you. The first steps for getting involved are relevant for everyone. If you’re new here, the first steps below will give you an idea of what is possible and where you may one day choose to volunteer. If you’re a pro, this is a massive opportunity for you to step up, hold a title, and hold the power to help direct the future of our community. 

## The first step in the new Mautic organization
Are you ready? This is the first step not for Mautic, but for you. This is where you take a long hard look at the organizational chart above and decide where you feel like your unique talents and skills are best suited. Once you’ve taken a minute and reflected on your interests and what excites you the next step is to learn a bit more about the particulars of the position. What does it mean to be a primary leader? What does it mean to be an assistant lead? Are you willing to make that commitment to your Mautic community? 

It’s not fair to ask those questions without giving you enough information to make an informed decision. Here are the specifics about what it means to be a primary or assistant leader. 

*Primary Leader*: The primary leader is the volunteer responsible for the working specific group. Each of the items listed above requires a primary and assistant leader.  The primary leader sets the agenda for the working group, works with other working group leaders to identify the best course of action and then creates a prioritized list of goals. They will also setup and hold the weekly working group meetings. The primary leader is also constantly looking for volunteers in their working group who might be capable and interested in also becoming a working group leader. 

*Assistant Leader*: The assistant leader in a working group volunteers their time to support the primary leader. They don’t necessarily maintain the level of responsibility but they are capable of stepping in whenever necessary. The assistant leader also serves as the official “note taker” for the group who keeps track of volunteer activity, completed items, scheduled items, and concerns from the group. 

Always remember power comes with responsibility. The greater the power within the community the greater the responsibility which comes along with the role. Each of these positions is usually held for a 6 month period, but can be extended by one additional period. After this time, due to the time and effort involved, we recommend alternating out of a primary leadership (or assistant leadership) role. This allows you a chance to recharge, and also encourages others in the community to step up and enjoy the chance to influence and make significant contributions to the community. 

## The Team Leaders & Assistant Team Leaders
There’s one additional level of role available within the Mautic organization for you to get involved. These positions are not for the faint-of-heart. They require additional time, strong communication skills, and an empathetic nature. You’ll need to be able to handle differences and resolve conflicts as they arise. These positions are the team leader and assistant team leader. As shown in the graphic above, the teams are: 

* Product 
* Community
* Events
* Law & Finance
* Marketing

Each of these teams is responsible for managing and overseeing all the working groups beneath them.  You need to be focused on the details, able to prioritize and summarize, and perhaps most importantly listen and delegate effectively. This is a huge responsibility and an incredible opportunity to truly make a difference. 

In order for these positions to be filled well and with the best possible candidate, these positions are voted on by the community members within the associated working groups. In this way each group of volunteers pick their own team leader and assistant team leader. Again each role is held for 6 months with the opportunity for a 6 month extension.  We will of course be constantly evaluating these processes and making changes where appropriate. If you have ideas or suggestions you’re welcome to join in the discussion and share your ideas!

## How to get started
Now comes the exciting part.Getting started. The first step is simple and shared previously, review the chart and determine in what capacity you might like to begin getting involved. For review, those roles are: 

* Working Group Volunteer
* Working Group Primary Leader
* Working Group Assistant Leader
* Team Leader
* Assistant Team Leader

The process for signing up to be involved in a working group or leading a working group is simple: Fill out this Mautic Working Group form. 

In the case of a team leader or assistant team leader you may nominate yourself (or someone else) for those positions by filling out the Team Leader Nomination form.