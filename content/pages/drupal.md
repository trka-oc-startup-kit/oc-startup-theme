# Drupal & Mautic



![](https://www.mautic.org/wp-content/uploads/2014/10/drupal_128.png)



![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)







### About Drupal

Drupal is an open source content management platform powering millions of websites and applications. It’s built, used, and supported by an active and diverse community of people around the world.

[Learn more about Drupal on their website](http://www.drupal.org).




### Integration Details



The Mautic module provides an easy way for Drupal site administrators to implement the Mautic tracking pixel into their website.

Download the module and install through the Drupal administrator panel as you would any other module. Once you've installed the module you need to enter the URL to your Mautic installation and you're done.

[Download](https://github.com/mautic/mautic-drupal)

https://www.youtube.com/watch?v=lWiN8xT5k0w