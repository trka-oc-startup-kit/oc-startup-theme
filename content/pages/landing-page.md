# Creating A Landing Page


One of the ways the businesses do this is by creating landing pages with specific call to actions. These call actions may simply be something like filling out a form, or clicking a button to download something, or something else in which to encourage them anonymously to enter more information. And they’re going to enter this information in a form.

So now what you have is an anonymously that fills in the form, this gives you more information about them typically those fields would be a name, email address, and maybe some other information about them. As a business you now have a lot more information about this individual now they’re no longer in anonymously you can now link all the pages they visited by their IP address to a specific name and email address. This is where information begins to come into play. Now that you hold an email address you can do a variety of tasks.

You can **look up their social networks** based on their publicly available information, you can **add them to specific lists**, and you can **begin drip flow marketing email campaigns**.

### Additional Resources for creating beautiful landing pages



	- Check out this blog article where we discuss some of the [features that make your landing page truly spectacular](https://www.mautic.org/blog/landing-page-love/).
	- Also read this article discussing [the importance of creating mobile landing pages](https://www.mautic.org/blog/mobile-landing-pages/).