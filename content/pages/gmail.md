# Gmail & Mautic
   ![](https://www.mautic.org/wp-content/uploads/2014/10/gmail_128.png)
    ![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)
      
### About Gmail
  Gmail is a free advertising-supported email service provided by Google. Users may access Gmail as secure webmail, as well as via POP3 or IMAP4 protocols. Gmail initially started as an invitation-only beta release on April 1, 2004 and it became available to the general public on February 7, 2007, though still in beta status at that time. The service was upgraded from beta status on July 7, 2009, along with the rest of the Google Apps suite.   

 [Learn more about Gmail on their website](http://www.gmail.com).  

   
### Integration Details
   Gmail is one of several SMTP protocols allowed within the Mautic system and comes by default integrated within every Mautic installation.   

  You can enable Gmail as your email service provider in the Configuration settings page.  Here you will enter your account details and other Gmail specific information.