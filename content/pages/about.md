#About Mautic Marketing automation

Mautic began with a single focus. Equality. The Mautic community believes in giving every person the power to understand, manage, and grow their business or organization. Mautic is focused on helping this belief become a reality by getting powerful marketing automation software into the hands of everyone.  


When David Hurley (@dbhurley) began Mautic he had a big goal. A plan to **move horizons**, and change the world. He foresaw Mautic as software made by the people and for the people and as such the community became a top priority and integral part. Those people interested in becoming involved in a community with a vision to change the world should consider getting involved in Mautic. People are the priority. Equality is the goal.

### What is Marketing Automation

The concept of marketing automation is not a new idea and the general idea of automated marketing is one which most are familiar with, though the terminology may be different. Here’s a very brief overview:

> Marketing Automation is a platform for saving time, eliminating errors, and improving efficiency for a wide range of marketing tasks across multiple channels.

If you’re interested in learning more there are some [excellent resources](https://www.mautic.org/marketing-automation-software/) available which give more background information to get you started.

### About Mautic.org

The Mautic.org website gives you the opportunity to [download and install](https://www.mautic.org/download/) a free, open source, marketing automation platform called Mautic. This software has been written to run on most standard hosting environments and requires very little in the way of time to install.

Because Mautic is open source the software is 100% customizable for your particular needs and situations. You can finally use a marketing automation platform the way your business runs instead of changing your business to fit a piece of software. There is a service available, Mautic.com, which gives you a [completely free hosted Mautic platform](https://www.mautic.org/demo/) in less than a minute. This free hosted solution offers a few different options from the version you download and configure on our own server but gives you a version of Mautic instantly available with no set-up or servers to configure.

### Get Involved In The Mautic Community

Of course because Mautic is built and grown as a result of the community involvement your participation is instrumental in its long-term success. You can quickly get involved in the forums or the mailing lists to begin. As you become more interested in diving deeper you can attend or volunteer to help establish or co-organize a [MauticMeetup](http://mautic.meetup.com)<sup>TM</sup> close to you.

### More Information

If you’re interested in learning more about Mautic or its founder, David, then dig in or join the [community on Slack](https://www.mautic.org/slack).