## Join the future of marketing

The future of marketing is automation. The power behind successful automation is people. The Mautic community is the future.







## Get Involved







### Download the Platform

[Download Now](/download)





### Contribute Code

[Start Contributing](/support)





### Join the Conversation

[Get Involved](/community)






![](/wp-content/themes/hueman-child/img/thumbnail/thumbnail_hurley.jpg)


### “Cutting edge marketing automation empowered by open source technology.This is the future; Mautic is leading the way.”

<span class="name">David Hurley</span> <span class="title">Founder, Mautic</span>  










## Want to Extend Mautic?

Find Mixins in the Marketplace to extend your Mautic even further. Browse, download, and add your new Mixin to Mautic. The power of open source changes everything.

[Browse the Mautic Marketplace](/marketplace)