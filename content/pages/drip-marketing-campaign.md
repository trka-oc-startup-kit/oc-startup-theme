# Drip Marketing Campaign


So let’s talk next about drip marketing campaigns. The whole idea behind a drip email campaign is that you’re going to be sending targeted emails to specific lists automatically on some kind of predefined schedules. There’s **two forms of campaigns**, the first is a **time-based** dripflow campaign, and the second is a **user-driven** dripflow campaign. Obviously you can do a mix of the two campaign styles or hybrid campaign quite easily. What do these campaigns actually look like?

![](https://medium2.global.ssl.fastly.net/max/2000/1*wjuiCpOUjnD_WiQ1FODAxw.png)


**Example:** Let’s say someone comes to your website they have browsed around a couple of different sections, and then they fill out a form on the landing page in order to receive a white paper about a specific product. Now you know first of all what pages they visited, the product of their interested in, and the resources they’ve downloaded. In your marketing automation tool you may set up a list for everyone who might fill out that form.

You can now **define specific campaigns** to send those leads on that list. **Targeted emails on a predefined schedule**. You may send them an email directly after they download the asset, or you may choose to wait a week (or longer).

Either way you can then set up a **[decision tree](https://www.mautic.org/marketing-automation-software/decision-tree-workflows/) (a great topic). **.