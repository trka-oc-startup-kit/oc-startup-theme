Mautic is thrilled to announce a partnership with [Slack](https://slack.com/), an all-in-one, real time team communications platform. We have found Slack to be well-integrated and effective, and we think it will be great for our community.

Slack provides seamless transitions in your work and communication between all of your devices.



 

Some of these features include:


 	- Open project, group and topic channels
 	- Built-In discoverability, history, and searches across all channels
 	- Messaging includes files and comments, inline images and video
 	- Dozens of integrations, like Twitter, Google Docs, GitHub, Dropbox, Trello, and Asana
 	- Customizable cross-device notifications
 	- Auto complete
 	- Apple & Google emoji styles



#### Tour

Take the Slack [tour](https://slack.com/is/team-communication) for a full description of the features and benefits, as well as tips for getting the most out of the your communication platform.

### Joining Mautic on Slack

It’s easy, enter your email below, click Get My Invite, then follow the prompts from the invitation email! Be sure to review our [Code of Conduct](https://www.mautic.org/code-of-conduct/), as it outlines our expectations for all those who participate in our community.

<iframe onload="this.style.display='block';" style="display: none; width: 50%; border: 1px solid #d5d4d4; margin: 40px auto; height: 300px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; padding: 30px;" src="https://www.mautic.org/slack/signup" width="300" height="150" scrolling="no"></iframe>

#### Native Applications

Slack is available for you to download as a native [Mac app](https://itunes.apple.com/us/app/slack/id803453959?mt=12), a full-feature [web app](https://slack.com/), and as [iOS](https://itunes.apple.com/us/app/slack-team-communication/id618783545?mt=8) or [Android](https://play.google.com/store/apps/details?id=com.Slack&hl=en) apps.

### Channels

Select the “Channels” List and scroll through the described channels to see which topics are appropriate for your contributions.

Just click “Join” and jump into the conversation!

#### Help and Support

Slackbot is always there to answer simple question, just send a Direct Message! For in-app help press the “?” button, next to the search box. Alternatively, you can visit the incredibly useful Slack Help Center [https://slack.zendesk.com/hc/en-us ](https://slack.zendesk.com/hc/en-us)

#### Configure Notifications

Mobile, desktop, and email notifications allow you to get notification where and when you want them. Limit or define the types of notifications you receive and set keyword triggers to stay updated in the right conversations.

#### Keyboard Shortcuts

Slack has lots of shortcuts to make it even easier to use. The full list can be accessed by pressing “Command”+ “?"

#### Customize with the Mautic Color Theme

Just paste `#1D232B,#4E5D9D,#171C22,#FFFFFF,#171C22,#F5F4F4,#35B5B9,#FDB933` into the the Preferences -> Sidebar Theme ->customize your theme

*** Pretty neat, right?***
![](https://www.mautic.org/wp-content/uploads/2015/01/Slack_customtheme.png)
  


We look forward to even more efficient and productive communications with all of you.

See you on Slack!