There are several ways you can find answers to your Mautic questions. But it's important to use the right channel for the question you have. Below are three different areas in the Mautic community that can help you.  

### Community Forums

If you have a question that may have been asked before, or one that you imagine might be helpful for others in the future the forums are a great place to start. Here you can review questions that have been asked as well as post your own.  


[Join Forums](/forums)

### Community Slack

If you are looking for a quick answer to a relatively simple question, or maybe just need someone to tell you where you should look, the Slack channels are a good resource. This is where live conversation happens and you can quickly get involved in Mautic (be sure to check out all the different channels) 

[Join Slack](/slack)

### Issue Reporting

If you have a more technical question, or if you believe you've found a bug in Mautic then the issue tracker is the best place to post your question or problem. Also, if you have a great idea for a new feature in Mautic this is also the first place to go.  

[Review Issues](https://www.github.com/mautic/mautic)