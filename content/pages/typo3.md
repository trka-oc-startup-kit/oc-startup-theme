# Typo3 & Mautic
   ![](https://www.mautic.org/wp-content/uploads/2014/10/typo3_128.png)
    ![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)
      
### About Typo3
  The biggest advantage of using a CMS (content management system) for your website is the ability to control your content separately from the layout or design. With a powerful CMS like TYPO3 CMS you can add, change and remove text, images and plug-ins on your site without installing any proprietary software or paying a third-party. Because of the simple administration and editing tools in TYPO3 CMS, you can run and edit any kind of site, personal or business, without learning HTML, CSS, programming, or web design. There are other CMS solutions out there, of course, but TYPO3 CMS has some unique features that make it a great option for websites of any size, from small to enterprise:   

 [Learn more about Typo3 on their website](http://www.typo3.org).  

   
### Integration Details
   The Mautic plugin provides an easy way for Typo3 site administrators to implement the Mautic tracking pixel into their website.   

Download the plugin and install through the Typo3 administrator panel as you would any other plugin. Once you've installed the plugin you need to enter the URL to your Mautic installation and you're done.    

 [Download](https://github.com/mautic/mautic-typo3/archive/master.zip)