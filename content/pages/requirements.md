<p class="lead">For an optimal Mautic experience, it is recommended your server or hosting provider meet the following requirements. In addition, verify your email service provider is capable of sending large volume emails. 
</p>


<table class="table table-bordered table-striped">
<thead>
<tr>
<th align="left">Software</th>
<th align="left">Minimum</th>
<th align="left">More information</th>
</tr>
</thead>
<tbody>
<tr>
<td>PHP (Magic Quotes GPC off)</td>
<td>5.6.19 + (including 7.0x)</td>
<td><a href="https://secure.php.net/" target="_blank">https://secure.php.net/</a></td>
</tr>
<tr>
<td colspan="4"><strong>Supported Databases:</strong></td>
</tr>
<tr>
<td>MySQL (InnoDB support required)</td>
<td>5.5.3 +</td>
<td><a href="https://www.mysql.com/" target="_blank">https://www.mysql.com/</a></td>
</tr>
<tr>
<td colspan="4"><strong>Supported Web Servers:</strong></td>
</tr>
<tr>
<td>Apache<sup><a href="#footnote-apache">[1]</a></sup></td>

<td>2.x +</td>
<td><a href="http://www.apache.org" target="_blank">http://www.apache.org</a></td>
</tr>
<tr>
<td>Nginx<sup><a href="#footnote-apache">[2]</a></sup></td>

<td>1.0 + (1.8 recommended)</td>
<td><a href="http://wiki.nginx.org/" target="_blank">http://wiki.nginx.org/</a></td>
</tr>
<tr>
<td>Microsoft IIS<sup><a href="#footnote-apache">[2]</a></sup></td>

<td>7</td>
<td><a href="http://www.iis.net" target="_blank">http://www.iis.net</a></td>
</tr>
</tbody>
</table>

<hr />
#### Footnotes
<small class="text-muted">
[1] In order to use SEO URLs, you will need to have the Apache mod_rewrite extension installed.  
[2] In order to use SEO URLs, extra configuration is required.
</small>