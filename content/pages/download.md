# Start using marketing automation today!


The latest release of Mautic is available as an installable package below. For another format or to see the files first, take a look at the [Github repository](https://github.com/mautic/mautic). <strong>Questions?</strong> Check out the [quick start guide](https://www.mautic.org/getting-started/) or head over to our [forum](https://www.mautic.org/community), or [Slack](http://www.mautic.org/slack) where you can interact with our community and find answers to many of the challenges you might face.

<hr />


<form id="mauticform_downloadmautic" action="https://m.mautic.org/form/submit?formId=5" autocomplete="off" method="post" target="mauticiframe_downloadmautic" data-mautic-form="downloadmautic">
<div class="mauticform-innerform">
<div id="mauticform_downloadmautic_error" class="mauticform-error"></div>
<div id="mauticform_downloadmautic_message" class="mauticform-message"></div>
<div class="row">
<div class="form-group col"><input id="mauticform_input_downloadmautic_firstname" class="form-control" name="mauticform[firstname]" type="text" value="" placeholder="First Name"></div>
<div class="form-group col"><input id="mauticform_input_downloadmautic_lastname" class="form-control" name="mauticform[lastname]" type="text" value="" placeholder="Last Name"></div>
</div>
<div class="grid one-half last">
<div class="form-group"><input id="mauticform_input_downloadmautic_emailaddress" class="form-control" name="mauticform[emailaddress]" type="email" value="" placeholder="Email Address (Required)"></div>
</div>
<div class="clear"></div>
<div class="grid one-half">
<div id="mauticform_downloadmautic_role" class="form-group mauticform-row mauticform-select mauticform-field-5 mauticform-required" data-validate="role" data-validation-type="select"><select id="mauticform_input_downloadmautic_role" class="form-control mauticform-selectbox" name="mauticform[role]">
<option value="">Choose your role...</option>
<option value="Marketing Executive">Marketing Executive</option>
<option value="Marketing Contributor">Marketing Contributor</option>
<option value="Technology Executive">Technology Executive</option>
<option value="Technology Contributor">Technology Contributor</option>
<option value="Software Engineer">Software Engineer</option>
<option value="Other Executive">Other Executive</option>
<option value="Other Contributor">Other Contributor</option>
</select></div>
</div>
<div><button class="btn btn-brand-purple" type="submit"><i id="download_button_icon"></i>Download Latest Version</button>
<input id="mauticform_downloadmautic_id" name="mauticform[formId]" type="hidden" value="5">
<input id="mauticform_downloadmautic_return" name="mauticform[return]" type="hidden" value="">
<input id="mauticform_downloadmautic_name" name="mauticform[formName]" type="hidden" value="downloadmautic"></div>
</div>
</form>
<iframe id="mauticiframe_downloadmautic" style="display: none; margin: 0; padding: 0; border: none; width: 0; height: 0;" name="mauticiframe_downloadmautic" width="300" height="150"></iframe>