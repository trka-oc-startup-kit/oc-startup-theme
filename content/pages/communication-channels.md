The Mautic community is a global community which can be found and joined through a number of ways. We have organized the different channels here so you can easily find them and get connected quick.   

     
#### Community
  Our community is a place to ask questions, give advice, and get to know others. Sign up now and begin getting more involved in the amazing Mautic community.   

[View Community](/community)  

         
#### Group Chats
 Mautic uses the awesomeness of Slack to provide quick chats between groups and interactions between people. Ask questions, give answers, or just hang out with friends in the community.   

 [View Chats](get-involved/group-chats)  

      
#### Mautic Blog
  The Mautic blog is a centralized location where you can find relevant posts from various parts of the Mautic community. Are you a writer? Consider volunteering with the blog.   

 [View Blog](/blog)  

        
#### MVP
  The Mautic Valuable Person represents a community member or even a team which has done something amazing. Remember these contributions are not always code related.  

 [Previous MVPs](#)  

      
#### Mailing List
  There are several different Mautic mailing lists you can use to stay up-to-date on issues, and posts associated with the community, including any potential security issues.  

 [View Mailing Lists](get-involved/mailing-lists)