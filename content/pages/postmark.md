# Postmark & Mautic
   ![](https://www.mautic.org/wp-content/uploads/2014/10/postmark_128.png)
    ![](https://www.mautic.org/wp-content/uploads/2014/09/Mautic_Logo_LB.png)
      
### About Postmark
  Postmark is specifically built to deliver your app’s emails to the inbox, fast. Instantly send and receive transactional email with zero maintenance.    

 Since we send transactional only, ISPs love us. Our infrastructure combined with proper content and delivery practices means higher response rates from your customers.  

 [Learn more about Postmark on their website](http://www.postmark.com).  

   
### Integration Details
   Postmark is one of several SMTP protocols allowed within the Mautic system and comes by default integrated within every Mautic installation.   

  You can enable Postmark as your email service provider in the Configuration settings page.  Here you will enter your account details and other Postmark specific information.