// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

const shared = require('./shared');

Cypress.Commands.add('login_form', (username, password) => {
	cy.visit('/login');
	cy.get('[name=login]')
		.type(username);
	cy.get('[name=password]')
		.type(password);
	cy.get('[type=submit]')
		.click()
	cy.url().should('contain', '/forum'); // <-- using this redirect to wait for login to finish. is there a better way?
	//--
});

Cypress.Commands.add('logout_link', ()=>{
	cy.get(shared.data_sel('usernav-toggle'))
		.click();
	cy.get(shared.data_sel('logout-link'))
		.click();
})

Cypress.Commands.add('find_own_first_topic', (username, password)=>{
	let user_forumpage = [
		'mauticians',
		username,
		'forum'
	].join('/')
	//-- login and go to own profile forum page
	cy.login_form(username, password)
	cy.url()
		.should('contain', '/forum');
	cy.visit(user_forumpage);
	//-- go to first topic
	cy.get('li.forum-post .media-heading a')
		.first()
		.click();
})
